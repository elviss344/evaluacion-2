<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mi primera página web</title>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <script type="text/javascript">
            function identificarAnimal(animal) {
                switch (animal) {
                    case 'caballo':
                        alert("El sonido lo emite un caballo");
                        break;
                    case 'gallo':
                        alert("El sonido lo hace un gallo");
                        break;
                    case 'gato':
                        alert("El sonido lo hace un gato");
                        break;
                    case 'pato':
                        alert("El sonido lo hace un pato");
                        break;
                    case 'perro':
                        alert("El sonido lo hace un perro");
                        break;
                    default:
                        alert("No se identifico el animal");
                }
            }
        </script>
    </head>
    <body background="resources/fondoo.jpg" style="background-size: cover">
        <h1  class="text-center text-black-50">Veterinaria</h1>
        <p  class="text-center text-text-black-50">animalitos de Elvis Crespo Lazo</p>
        <hr class="my-4">


        <div class="container">
            <div class="jumbotron">
                <div class="row justify-content-md-center mb-3">
                    <div class="col-md-4">
                        <h1 class="text-center text-black-50">seccion caballo</h1>
                        <div class="card">
                            <img src="resources/caballo.jpg" class="img-thumbnail">
                            <div class="card-body">
                                <h5 id="nombreAnimal" class="card-title">Caballo</h5>
                                <p class="card-text">pegaso</p>
                            </div>
                        </div>
                        <audio id="relinchar" src="resources/sonidos/relinchar.mp3"></audio>
                        <a class= "btn btn-primary btn-lg " onclick="document.getElementById('relinchar').play(); identificarAnimal('caballo');" class="btn btn-primary">¡precione!</a>
                    </div>


                    <div class="col-md-4">
                        <h1 class="text-center text-black-50">seccion gallo</h1>
                        <div class="card">
                            <img src="resources/gallo.jpg" class="img-thumbnail">
                            <div class="card-body">
                                <h5 id="nombreAnimal" class="card-title">gallo</h5>
                                <p class="card-text">claudio</p>  
                            </div>
                        </div>
                        <audio id="cacarear" src="resources/sonidos/cacarear.mp3"></audio>
                        <a class= "btn btn-primary btn-lg " onclick="document.getElementById('cacarear').play(); identificarAnimal('gallo');" class="btn btn-primary">¡precione!</a>
                    </div>


                </div>
                <div class="row mb-2">
                    <div class="col-md-4">
                        <h1 class="text-center text-black-50">seccion gato</h1>
                        <div class="card">
                            <img src="resources/gato.png" class="img-thumbnail">
                            <div class="card-body">
                                <h5 class="card-title">michi</h5>
                                <p class="card-text">misifus</p> 
                            </div>
                        </div>
                        <audio id="maullido" src="resources/sonidos/maullido.mp3"></audio>
                        <a class= "btn btn-primary btn-lg " onclick="document.getElementById('maullido').play(); identificarAnimal('gato');" class="btn btn-primary">¡precione!</a>
                    </div>


                    <div class="col-md-4">
                        <h1 class="text-center text-black-50">seccion pato</h1>
                        <div class="card">
                            <img src="resources/pato.jpg" class="img-thumbnail">
                            <div class="card-body">
                                <h5 class="card-title">Pato</h5>
                                <p class="card-text">Patocar buenanota</p>
                            </div>
                        </div>
                        <audio id="graznar" src="resources/sonidos/graznido.mp3"></audio>
                        <a class= "btn btn-primary btn-lg " onclick="document.getElementById('graznar').play(); identificarAnimal('pato');" class="btn btn-primary">¡precione!</a>
                    </div>

                    <div class="col-md-4">
                        <h1 class="text-center text-black-50">seccion perro</h1>
                        <div class="card">
                            <img src="resources/perro.jpg" class="img-thumbnail">
                            <div class="card-body">
                                <h5 class="card-title">Perro</h5>
                                <p class="card-text">guardian</p>
                            </div>
                        </div>
                        <audio id="ladrar" src="resources/sonidos/ladrar.mp3"></audio>
                        <a class= "btn btn-primary btn-lg " onclick="document.getElementById('ladrar').play(); identificarAnimal('perro');" class="btn btn-primary">¡precione!</a>
                    </div>


                </div>
            </div>
        </div>
    </body>
</html>
